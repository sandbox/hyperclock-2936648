legal-generator
===============

this module has been taken and modded from the [legal-gen]() module. 

The [legal-gen]() module was made for 7.x and seems to have been marked as
"Not supported (i.e. abandoned), and no longer being developed."

Legal Generator will be built (modded) for Drupal 8.x and above.

I plan on adding more example templates, such as:

  * Imprint
  * Disclaimer
  * Trademarks
  * Copyright
  * License
  
This examples will be aimed at Open Source Projects, but should be easy to change.
